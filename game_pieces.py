class ship:
    '''Creates a ship object that knows it name, and length,
    and can keep track of how many times it has been hit and
    whether or not it is dead'''

    def __init__(self, length, name):
        self.name = name
        self.length = length
        self.hits = 0
        # Format?
        self.isNotDead = True

    def __repr__(self):
        return self.name

    def getName(self):
        return self.name

    def getLength(self):
        return self.length

    def isAlive(self):
        return self.isNotDead

    def hit(self):
        '''When called, this method tells the ship
        that it has been hit and sets is status to
        dead if the hit killed it.'''
        self.hits += 1
        if self.hits >= self.length:
            self.isNotDead = False


class square:
    '''Creates a square for the board. Each board has
    coordinates (in (r,c) form), a hitMissNeither
    variable to track whether it has been fired on,
    and can refer to a ship that occupies the space'''

    def __init__(self, coordinates):
        self.ship = None
        self.isHit = False
        self.coordinates = coordinates
        self.hitMissNeither = 'Neither'

    def __repr__(self):
        return '(%i, %i)' % (self.coordinates[0], self.coordinates[1])

    def getHitMissNeither(self):
        return self.hitMissNeither

    def getShip(self):
        return self.ship

    def getIsHit(self):
        return self.isHit

    def hasShip(self):
        '''Returns True if a ship occupies the square
        and false if no ship does'''
        # Self.ship is none if the square has no ship.
        # Otherwise, it is the ship's name
        if self.ship == None:
            return False
        else:
            return True

    def setHitMissNeither(self, HMN):
        self.hitMissNeither = HMN

    def setShip(self, ship):
        '''self.ship now points to the ship that occupies this space'''
        self.ship = ship

    def hit(self):
        '''Records whether an incoming shot is a hit or miss and
        hits relevant ships. Also returns whether or not there is
        a ship in the square (to be used by the enemy's fire function)'''

        # If it hasn't already been hit. This prevents a player from killin
        # a ship by firing at the same square multiple times
        if not self.isHit:
            self.isHit = True

            # If it has a ship, hit the ship, record it for the interface and return True for hit
            if self.ship != None:
                self.ship.hit()
                self.hitMissNeither = 'Hit'
                return True

            # Otherwise, record the miss for the interface, and return False for miss
            else:
                self.hitMissNeither = 'Miss'
                return False
        else:
            return True