from graphics import *

class GUI:
    '''A click-based GUI for Battleship'''

    def __init__(self):
        # Initializes what will be lists that help
        # getTarget and placeShip match areas of the
        # screen to squares
        self.topBoardRef = None
        self.lowBoardRef = None

        # Creates a ClickableWindow as defined in modTypeAndClick.
        self.windowWidth = 600
        self.windowHeight = 800
        self.window = GraphWin('Battleship', self.windowWidth, self.windowHeight)

        # The curtain is used to obscure irrelevant objects.
        # There is probably a far better way to do this.
        self.curtain = Rectangle(Point(0, 0), Point(self.windowWidth, self.windowHeight))
        self.curtain.setOutline('black')
        self.curtain.setFill('black')

        # Creates the Title
        self.title = Text(Point(300, 50), 'BATTLESHIP')
        self.title.setFill('green')
        self.title.setSize(36)
        self.title.setFace('courier')

        # Defines where the two boards are drawn on the screen.
        self.ownBoardStart = Point(200, 410)
        self.enemyBoardStart = Point(200, 130)
        self.messages = []

    def closed(self):
        '''This function can be used to keep the game
        from crashing if the graphics window is closed'''
        if self.window.closed:
            return True
        else:
            return False


    def getSquare(self, player, ref):
        '''This function uses a reference board to translates
        a mouse click into the coordinates of a square in (r,c) form'''



        squareCoordinates = None
        while squareCoordinates == None:
            clickedPoint = self.window.getMouse()

            squareX = clickedPoint.getX()
            squareY = clickedPoint.getY()

            for entry in ref:
                # The board reference contains the lowest and highest
                # x and y coordinates of each square on the board.
                if (entry[1][0] < squareX < entry[1][1]) and \
                        (entry[2][0] < squareY < entry[2][1]):
                    squareCoordinates = (entry[0][0], entry[0][1])

       # Returns the coordinates of a square in (r,c) form
        return squareCoordinates

    def obscure(self):
        '''This function draws over all displayed objects.
        If I understood how to properly undraw objects I would
        do that instead.'''

        for item in self.window.items:
            if item != self.curtain and item != self.title:
                item.undraw()

        # Creates the title
        self.title = Text(Point(300, 50), 'BATTLESHIP')
        self.title.setFill('green')
        self.title.setSize(36)
        self.title.setFace('courier')

        # Creates a new curtain
        self.curtain = Rectangle(Point(0, 0),
                                 Point(self.window.getWidth(), self.window.getHeight()))
        self.curtain.setOutline('black')
        self.curtain.setFill('black')

        # Draws the new curtain and the new title
        self.curtain.draw(self.window)
        self.title.draw(self.window)

    def makeBlank(self):
        '''This UI does not use this function'''
        pass

    def poseQuestion(self, atBottom, question, options):
        '''This function asks a question and returns the response.
        atBottom is a Boolean that indicates where the question should
        be drawn, question is a string containing the question, and
        options is a tuple containing the possible answers'''

        # Most questions are displayed in a font size that contains
        # 20 px per line
        pxPerLine = 20

        linesInQuestion = 1
        # When asking the User to select a ship, the question itself has
        # two lines
        if '\n' in question:
            linesInQuestion += 1
            # This question also uses a smaller font size
            pxPerLine = 16

        # Creates a single string to draw.
        message = question
        for option in options:
            message = message + '\n' + option

        # Determines where to draw the string and draws it.
        if atBottom:
            self.lowMessage(message)
            startPoint = Point(310, 700)
        else:
            self.highMessage(message)
            startPoint = Point(30, 450)

        # Determines how many lines of text are displayed on the screen
        lines = linesInQuestion + len(options)
        # Determines at what Y value the options start
        optionsStartY = startPoint.getY() - (lines * pxPerLine) // 2 + linesInQuestion * pxPerLine

        # Prevents a previously clicked point from being used.
        answer = None
        while answer == None:
            answerpoint = self.window.getMouse()
            if (optionsStartY < answerpoint.getY() < optionsStartY + len(options) * pxPerLine):
                answer = options[(answerpoint.getY() - optionsStartY) // pxPerLine]

        return answer

    def highMessage(self, text):
        '''Prints a message about halfway down the window.'''
        question = Text(Point(300, 450), text)
        question.setFill('green')
        question.setSize(14)
        question.setFace('courier')
        question.draw(self.window)

    def showTitle(self):
        '''Shows the title screen'''

        self.makeBlank()
        self.curtain.draw(self.window)

        # The title on the title screen is lower than the normal title
        tempTitle = Text(Point(300, 150), 'BATTLESHIP!')
        tempTitle.setFill('green')
        tempTitle.setSize(36)
        tempTitle.setFace('courier')

        # Battleship really is largely about guesswork.
        subTitle = Text(Point(300, 450), 'A GAME OF NAVAL STRATEGY!\n(and a bit of shoddy guesswork)')
        subTitle.setFill('green')
        subTitle.setSize(24)
        subTitle.setFace('courier')
        subTitle.draw(self.window)

        tempTitle.draw(self.window)

        # Waits for the user to click anywhere.
        self.lowMessage('Click anywhere to continue.')
        self.window.getMouse()

    def determineGameMode(self):
        '''Sets the gamemode to either Classic or Salvo mode.
        In Salvo mode you get one shot for each of your
        living ships.'''

        self.makeBlank()
        self.obscure()

        # False means that the question is not displayed at the bottom of the screen.
        response = self.poseQuestion(False, 'What type of game would you like to play?', ('Classic', 'Salvo'))

        # The game defaults to Classic mode
        if len(response) > 0 and response[0] == 'S':
            return 'Salvo'
        else:
            return 'Classic'

    def playerTypes(self):
        '''Determines whether each player is AI or Human'''

        self.makeBlank()
        self.obscure()

        response = self.poseQuestion(False, 'What type of player should player 1 be?', ('Human', 'AI'))

        # The game defaults to AI players.
        if (len(response) > 0 and response[0] == 'H') \
                or (len(response) > 2 and response[:3] == 'A H'):
            player1Type = 'Human'
        else:
            player1Type = 'Computer'

        self.makeBlank()
        self.obscure()

        response = self.poseQuestion(False, 'What type of player should player 2 be?', ('Human', 'AI'))

        if (len(response) > 0 and response[0] == 'H') \
                or (len(response) > 2 and response[:3] == 'A H'):
            player2Type = 'Human'
        else:
            player2Type = 'Computer'

        return player1Type, player2Type

    def playerName(self, playerString):
        '''Asks the player for their name, in order to bring a more
        personal touch to the game'''

        return playerString

    def lowMessage(self, text):
        '''Prints a message at the bottom of the screen'''

        messageCurtain = Rectangle(Point(0, self.window.getHeight() - 180),
                                   Point(self.window.getWidth(), self.window.getHeight()))
        messageCurtain.setOutline('black')
        messageCurtain.setFill('black')

        message = Text(Point(310, 700), text)
        # Some messages need to be smaller than others in order to fit
        message.setFill('green')
        if len(text) < 65:
            message.setSize(15)
        else:
            message.setSize(12)
        message.setFace('courier')
        if not self.closed():
            messageCurtain.draw(self.window)
            message.draw(self.window)
        self.messages.append(message)

    def printBoard(self, board, startPoint):
        '''draws a given board on the screen.
        Does not obscure first'''

        xStart = startPoint.getX()
        yStart = startPoint.getY()
        xCounter = 0
        yCounter = 0
        objectsToDraw = []
        boardReference = []

        WH = 19
        hmOffset = 10
        hmSize = 14

        # This draws the board and row letters
        for r in range(len(board)):
            xCounter = 0
            yCounter += WH

            # Prints the row labels.
            # The labeling code takes advantage of
            # int to ASCII conversion to print the row labels
            sideLabel = Text(Point(startPoint.getX() - hmOffset,
                                   startPoint.getY() + yCounter + hmOffset),
                             chr(yCounter // WH + 64))
            sideLabel.setFill('green')
            sideLabel.setFace('courier')
            sideLabel.setSize(hmSize - 2)
            objectsToDraw.append(sideLabel)

            # Prints the board, row by row.
            for c in range(len(board[r])):

                # Prints the Column Labels
                if yCounter == WH:
                    topLabel = Text(Point(startPoint.getX() + xCounter + hmOffset,
                                          startPoint.getY() + yCounter - hmOffset),
                                    str(xCounter // WH + 1))
                    topLabel.setFill('green')
                    topLabel.setFace('courier')
                    topLabel.setSize(hmSize - 2)
                    objectsToDraw.append(topLabel)

                # Determines where the current square is to be drawn
                point1 = Point(xStart + xCounter, startPoint.getY() + yCounter)
                point2 = Point(point1.getX() + WH, point1.getY() + WH)
                squareGraphic = Rectangle(point1, point2)
                squareGraphic.setOutline('green')
                objectsToDraw.append(squareGraphic)

                # If there is a ship in the square, color the square gray
                if board[r][c].getShip() != None:
                    squareGraphic.setFill(color_rgb(125, 125, 125))

                # If a ship in the square has been hit, draw an X in the square
                if board[r][c].getHitMissNeither() == 'Hit':
                    x = Text(Point(point1.getX() + hmOffset, point1.getY() + hmOffset), 'X')
                    x.setFill('green')
                    x.setSize(hmSize)
                    objectsToDraw.append(x)

                # Otherwise if the square has been hit, draw an O on the square
                if board[r][c].getHitMissNeither() == 'Miss':
                    o = Text(Point(point1.getX() + hmOffset, point1.getY() + hmOffset), 'O')
                    o.setFill('green')
                    o.setSize(hmSize)
                    objectsToDraw.append(o)

                # Appends a list to the boardReference stating where the square starts and ends
                # on the graphics window.
                boardReference.append([(r, c),
                                       (point1.getX(), point2.getX()),
                                       (point1.getY(), point2.getY())])

                xCounter += WH

        # Draws all of the objects to draw the board.
        for item in objectsToDraw:
            item.draw(self.window)

        # returns boardReference so that getSquare can use it.
        return boardReference

    def showBoard(self, player):
        """Prints both of a player's boards"""
        self.makeBlank()
        self.obscure()

        # Labels the enemyBoard
        topLabel = Text(Point(self.enemyBoardStart.getX() + 80,
                              self.enemyBoardStart.getY() - 25),
                        "\n%s's board:" % (player.opponent))
        topLabel.setFill('green')
        topLabel.setSize(18)
        topLabel.setFace('courier')
        if not self.closed():
            topLabel.draw(self.window)

        # Labels the ownBoard
        bottomLabel = Text(Point(self.ownBoardStart.getX() + 80,
                                 self.ownBoardStart.getY() - 25),
                           "\n%s's board:" % (player))
        bottomLabel.setFill('green')
        bottomLabel.setSize(18)
        bottomLabel.setFace('courier')
        if not self.closed():
            bottomLabel.draw(self.window)

            # Prints the boards and keep the board reference lists.
            self.topBoardRef = self.printBoard(player.getEnemyBoard(), self.enemyBoardStart)
            self.lowBoardRef = self.printBoard(player.getOwnBoard(), self.ownBoardStart)

    def findShip(self, shipName, player):
        '''Takes a ship's name and returns the ship.
        This function is used by selectShip'''

        for ship in player.getUnplacedShips():
            possibleMatchName = ship.getName()
            possibleMatchName = possibleMatchName.upper()
            if possibleMatchName[0] == shipName[0]:
                return ship

    def selectShip(self, player):
        '''Prompts a user to choose which ship to place.
        Returns the ship.
        Used by placeShip.'''

        message = "%s. Which ships would you like to place?\nYour options are ..." % (player.getName())

        # Creates a list to store the names of each unplaced ship
        shipNameList = []

        for ship in player.getUnplacedShips():
            # Prints the name and length of each unplaced ship and adds
            # it to the shipNameList
            # message = message +'\n'+"%s (length: %i)" %(ship, ship.getLength())
            shipNameList.append(ship.getName() + ' (length: %i)' % (ship.getLength()))

        response = self.poseQuestion(True, message, shipNameList)

        # Checks to see if the response is valid and promts the user
        # to try again if the previous response was invalid
        while (len(response) < 1) or (response[0] not in shipNameList):
            message = "Please Try again %s. Which ships would you like to place?\nYour options are ..." % (
            player.getName())
            for ship in player.getUnplacedShips():
                # Prints the name and length of each unplaced ship and adds
                # it to the shipNameList
                message = message + '\n' + "%s (length: %i)" % (ship, ship.getLength())
                shipNameList.append(ship.getName().upper()[0])
            if not self.closed:
                self.lowMessage(message)

        # Once the program has a valid response, it finds and returns the ship
        ship = self.findShip(response, player)
        return ship

    def placeShip(self, player):
        '''Prompts the user to place a ship and returns the ship, the
        place it has been placed, and the direction it faces.'''

        # Prompts the user to select a ship.
        ship = self.selectShip(player)

        # Prompts the user to enter a ship starting location and a direction

        # Keeps track of whether a placement has already failed.
        firstAttempt = True

        square1, square2 = None, None

        # If the squares don't exist, or if they are not in the same row or columns
        # or if they are the same square, the user is prompted to choose two squares.
        while (square1 == None) or \
                not ((square2[0] == square1[0] or square2[1] == square1[1]) and \
                             (not (square2[0] == square1[0] and square2[1] == square1[1]))):

            # Lets the user know whether or not a previous attempt at placement has failed.
            if firstAttempt:
                self.lowMessage(
                    'Click on the square that the %s should start on\nand the square that the %s should end on.' % (
                    ship, ship))
                firstAttempt = False
            else:
                self.lowMessage(
                    'Please try again.\nClick on the square that the ship should start on\nand the square that the ship should end on.')

            # Translates the mouseClicks into coordinates of a square in (r,c) form
            square1 = self.getSquare(player, self.lowBoardRef)
            square2 = self.getSquare(player, self.lowBoardRef)

        # Translates from the 'two square' input form used here to the
        # 'start and direction' form that the rest of the program uses.
        if square1[0] == square2[0]:
            if square1[1] > square2[1]:
                direction = 'L'
            else:
                direction = 'R'
        else:
            if square1[0] > square2[0]:
                direction = 'U'
            else:
                direction = 'D'

        # Once a valid set of squares is entered, the ship, starting square, and direction
        # are returned
        return ship, square1, direction

    def getTarget(self, player, shotsLeft):
        '''Prompts the player to enter a target, runs
        the result through the converter and returns
        it as an (r, c) tuple.'''

        if shotsLeft > 1:
            self.lowMessage("Click on your enemy's board to fire %s. \nYou have %s shots left" % (player, shotsLeft))
        else:
            self.lowMessage("Click on your enemy's board to fire, %s." % (player))

        target = self.getSquare(player, self.topBoardRef)

        return target

    def beenClicked(self):
        '''This function is used by endTurn, startTurn, and showTitle
        to wait for a mouse click.'''
        if self.window.lastClickedPoint == None:
            return False
        else:
            return True

    def endTurn(self, player):
        '''Prompts the player to end their turn'''
        self.lowMessage('%s, Click anywhere to end your turn.' % (player))
        self.window.getMouse()

    def startTurn(self, player):
        '''Prompts the player to end their turn.
        Used in Human vs Human games'''
        self.lowMessage('%s, Click anywhere to start your turn.' % (player))
        self.window.getMouse()

    def victory(self, player):
        '''Prints the victory screen'''

        self.makeBlank()
        self.obscure()

        # Lets you see everyone's board
        message = Text(Point(self.enemyBoardStart.getX() + 80,
                             self.enemyBoardStart.getY() - 25),
                       "\n%s's board:" % (player))
        message.setFill('green')
        message.setSize(18)
        message.setFace('courier')
        message.draw(self.window)

        message = Text(Point(self.ownBoardStart.getX() + 80,
                             self.ownBoardStart.getY() - 25),
                       "\n%s's board:" % (player.opponent))
        message.setFill('green')
        message.setSize(18)
        message.setFace('courier')
        message.draw(self.window)

        self.lowMessage("\n%s has won the game!\nClick anywhere to continue" % (player))

        self.printBoard(player.ownBoard, self.enemyBoardStart)
        self.printBoard(player.opponent.ownBoard, self.ownBoardStart)

        # Waits for the user to click somewhere.
        self.window.getMouse()

    def askYN(self, question):
        '''Asks the user a yes/no question. This function
        Asks a y/n question at the bottom of the window.
        It mainly exists for compatability purposes.'''

        response = self.poseQuestion(True, question, ('Yes', 'No'))

        if (len(response) > 0) and (response[0] == 'Y'):
            return True
        else:
            return False