from converter import *

class textUI:
    '''A Text-based UI for Battleship'''

    def __init__(self):
        self.language = 'English'
        self.converter = converter()

    def __repr__(self):
        return "Text-based UI for Battleship"

    def closed(self):
        # As long as the text inteface can respond
        # to this question it isn't closed.
        return False

    def showTitle(self):
        '''Displayes the "title screen"'''
        print("\nBATTLESHIP!")
        print("A GAME OF NAVAL STRATEGY!")
        # Battleship really is largely about luck.
        print("(and a bit of shoddy guesswork)\n\n")

        input("Press enter to continue\n>>> ")

    def determineGameMode(self):
        '''Sets the gamemode to either Classic or Salvo mode.
        In Salvo mode you get one shot for each of your
        living ships. Defaults to classic blank or
        unrecognizable input.'''

        print("\nWould you like to play a Classic or Salvo game?\n(In a Salvo game you have 1 shot per live ship.)")
        response = input('>>> ').upper()

        # The game defaults to Classic mode
        if len(response) > 0 and response[0] == 'S':
            return 'Salvo'
        else:
            return 'Classic'

    def playerTypes(self):
        '''Determines whether each player is AI or Human.
        Defaults to AI players'''

        print("\nShould player 1 be a human or an AI?")
        response = input('>>> ').upper()

        # The game defaults to AI players.
        if (len(response) > 0 and response[0] == 'H') \
                or (len(response) > 2 and response[:3] == 'A H'):
            player1Type = 'Human'
        else:
            player1Type = 'Computer'

        # It would probably have been better to make a
        # function that didn't duplicate code but was
        # called twice by gameManager
        print("\nShould player 2 be a human or an AI?")
        response = input('>>> ').upper()

        if (len(response) > 0 and response[0] == 'H') \
                or (len(response) > 2 and response[:3] == 'A H'):
            player2Type = 'Human'
        else:
            player2Type = 'Computer'

        return player1Type, player2Type

    def playerName(self, playerString):
        '''Asks the player for their name, in order to bring a more
        personal touch to the game'''
        return input('\nWhat is your name, %s?\n>>> ' % (playerString))

    def lowMessage(self, message):
        '''Prints a message.
        This function is more important in the hybridUI'''
        print(message)

    def obscure(self):
        '''This function is used in Human vs Human games
        to prevent one player from accidentally seeing the
        other player's board'''
        for i in range(200):
            print('---------------------------------------------------')

    def printBoard(self, board):
        '''Prints a text representation of a given board'''

        # The numbers for the columns in the board
        print('  1 2 3 4 5 6 7 8 9 10')

        # This draws the board and row letters
        for i in range(len(board)):
            # Before each row of the board is printed, its
            # label is printed.
            print('ABCDEFGHIJK'[i], end=' ')

            # Prints the board, row by row.
            for square in board[i]:

                # If the below condition is true, the bord has nott
                # been fired on.
                if square.getHitMissNeither() == 'Neither':
                    # Since only the ownBoards have ships, a player
                    # will not be able to see the enemy's ships.
                    if square.getShip() != None:
                        print(square.getShip().getName()[0], end=' ')
                    else:
                        print('-', end=' ')
                # If the square has been hit, the program prints H or M
                elif square.getHitMissNeither() == 'Hit':
                    print('X', end=' ')
                elif square.getHitMissNeither() == 'Miss':
                    print('O', end=' ')
            # This prevents the next label from being drawn on the wrong line.
            print(' ')

    def showBoard(self, player):
        """Prints both of a player's boards"""

        print("\n%s's Board:" % (player.opponent))
        self.printBoard(player.getEnemyBoard())
        print("\nYour Fleet:")
        self.printBoard(player.getOwnBoard())

    def findShip(self, shipName, player):
        '''Takes a ship's name and returns the ship.
        This function is used by selectShip'''

        for ship in player.getUnplacedShips():
            possibleMatchName = ship.getName()
            possibleMatchName = possibleMatchName.upper()
            if possibleMatchName[0] == shipName[0]:
                return ship

    def selectShip(self, player):
        '''Prompts a user to choose which ship to place.
        Returns the ship.
        Used by placeShip.'''

        # Creates a list to store the names of each unplaced ship
        shipNameList = []
        print("\n%s. Which ships would you like to place?" % (player.getName()))
        print("Your options are ...")
        for ship in player.getUnplacedShips():
            # Prints the name and length of each unplaced ship and adds
            # it to the shipNameList
            print("%s (length: %i)" % (ship, ship.getLength()))
            shipNameList.append(ship.getName().upper()[0])

        response = input('\n>>> ').upper()

        # Checks to see if the response is valid and promts the user
        # to try again if the previous response was invalid
        while (len(response) < 1) or (response[0] not in shipNameList):
            print("Please try again.")
            print("Your options are ...")
            for ship in player.getUnplacedShips():
                print("%s (length: %i)" % (ship, ship.getLength()))
            response = input('\n>>> ').upper()

            # Once the program has a valid response, it finds and returns the ship
        ship = self.findShip(response, player)
        return ship

    def placeShip(self, player):
        '''Prompts the user to place a ship and returns the ship, the
        place it has been placed, and the direction it faces.'''

        # Prompts the user to select a ship.
        ship = self.selectShip(player)

        # Prompts the user to enter a ship starting location and a direction
        print("\nWhere would you like to place the ship?")
        print("Please enter a square and a direction(r, l, u, d) \nExample: A3 r")
        response = input('\n>>> ').upper()

        # If the length of the response is too short, direction is set to None
        # so that another part of the program will reject the given placement
        if len(response) >= 2:
            direction = response[-1]
        else:
            direction = None

        # Converts the response to a (r,c) tuple.
        # The converter returns None if it can't convert
        # the input
        # response[:-1] sends only the coordinate to the converter
        start = self.converter.convert(response[:-1])

        # If the input is invalid, the user is prompted to try again.
        while (start == None) or (direction not in 'RULDruld'):
            print("\nPlease try again.")
            print("Where would you like to place the ship?")
            print("Please enter a start and a direction(r, l, u, d)")

            response = input('\n>>> ').upper()

            if len(response) >= 2:
                direction = response[-1].lower()
            else:
                direction = None

            start = self.converter.convert(response[:-1])

        # Once a valid square is entered, the ship, start, and direction
        # are returned
        return ship, start, direction

    def getTarget(self, player, shotsLeft):
        '''Prompts the player to enter a target, runs
        the result through the converter and returns
        it as an (r, c) tuple.'''

        if shotsLeft > 1:
            print("\n%s. Enter a target. You have %s shots left" % (player, shotsLeft))
        else:
            print("\nEnter a target, %s." % (player))
        response = input('>>> ').upper()

        # The converter takes the alphanumeric input
        # and translates it into an (r,c) tuple
        target = self.converter.convert(response)

        # If the converter is unable to translate
        # the input, it returns None.
        while target == None:
            if shotsLeft > 1:
                print("\nEnter a target, %s. You have %s shots left" % (player, shotsLeft))
            else:
                print("\nEnter a target, %s." % (player))
            response = input('>>> ').upper()
            # print response,
            target = self.converter.convert(response)
            # print(target)

        return target

    def endTurn(self, player):
        '''Prompts the player to end their turn'''

        print("\n%s, press Enter to end your turn." % (player))
        input('>>> ')

    def startTurn(self, player):
        '''Prompts the player to end their turn.
        Used in Human vs Human games'''

        print("\n%s, press Enter to start your turn." % (player))
        input('>>> ')

    def victory(self, player):
        '''Prints the victory screen'''

        print("\n%s has won the game!" % (player))

        # Lets you see everyone's board
        print("\n%s's board:" % (player))
        self.printBoard(player.ownBoard)

        print("\n%s's board" % (player.opponent))
        self.printBoard(player.opponent.ownBoard)

        input('\nPress Enter to continue\n>>> ')

    def askYN(self, question):
        '''Asks the user a yes/no question'''

        print("\n%s (y/n)" % (question))
        response = input('>>> ').upper()
        if (len(response) > 0) and (response[0] == 'Y'):
            return True
        else:
            return False