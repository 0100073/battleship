# README #

This program is a version of the classic board game Battleship, written in Python 3, which supports
both local multiplayer and playing against an automated opponent. 
The user can choose between a CLI and a GUI based on the Tkinter graphics module at startup.
To run the program, run battleshipMain.py with no command line arguments.