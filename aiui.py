from random import randrange
from converter import converter

class AIBrain:
    '''This class acts as the AI\'s "user interface"
    and contains most of the AI logic.'''

    def __init__(self, prioritySquares, favoriteSquares, unfiredSquares):
        # The square lists are determined by AIPlayer's __init__
        # and given to AIBrain's __init__ by gameManager
        self.prioritySquares = prioritySquares
        self.favoriteSquares = favoriteSquares
        self.unfiredSquares = unfiredSquares
        self.converter = converter()

    def __repr__(self):
        return "Battleship CPU AI"

    # Having these blank functions allows gameManager
    # to treat Humans and AI's equally
    def showBoard(self, player):
        '''The AI does not use this feature.
        This function allows the game manager
        to treat Humans and AI's the same.'''
        return None

    def lowMessage(self, message):
        '''The AI does not use this feature.
        This function allows the game manager
        to treat Humans and AI's the same.'''
        return None

    def startTurn(self, player):
        '''The AI does not use this feature.
        This function allows the game manager
        to treat Humans and AI's the same.'''
        return None

    def endTurn(self, player):
        '''The AI does not use this feature.
        This function allows the game manager
        to treat Humans and AI's the same.'''
        return None

    def obscure(self):
        '''The AI does not use this feature.
        This function allows the game manager
        to treat Humans and AI's the same.'''
        return None

    def closed(self):
        '''The AI does not use this feature.
        This function allows the game manager
        to treat Humans and AI's the same.'''
        return False

    def placeShip(self, player):
        '''Attempts to place a random ship in a
        random square with a random location.'''

        # Selects a random ship from the ships available
        ship = player.getUnplacedShips()[randrange(len(player.getUnplacedShips()))]

        # Selects a random direction
        direction = 'RULD'[randrange(4)]

        # Selects a random starting square
        start = (randrange(10), randrange(10))

        # These may be invalid, but gameManager will just let it
        # return invalid combinations until is stumbles onto a
        # valid one.
        return ship, start, direction

    def findAdjacentSquare(self, board, start, direction):
        '''Determines whether or not there is a square adjacent to
        the current square in the given direction and returns the
        square if one exists.
        This function is used by the thinkAbout function'''

        if (direction == 'R') and (start[1] < 9) and (board[start[0]][start[1]].getShip() != None):
            return (start[0], start[1] + 1)
        elif (direction == 'L') and (start[1] > 0) and (board[start[0]][start[1]].getShip() != None):
            return (start[0], start[1] - 1)
        elif (direction == 'U') and (start[0] > 0) and (board[start[0]][start[1]].getShip() != None):
            return (start[0] - 1, start[1])
        elif (direction == 'D') and (start[0] < 9) and (board[start[0]][start[1]].getShip() != None):
            return (start[0] + 1, start[1])
        else:
            return None

    def thinkAbout(self, player, target):
        '''Determines which squares to place in prioritySquares'''

        # This contains a bit of a hack where it directly checks to see if the square is a hit
        # Otherwise, thinkAbout would need prompting from gameManager
        if player.enemyBoard[target[0]][target[1]].getShip != None:
            for direction in 'RULD':

                # Gets the adjacent square if one exists
                adjacentSquare = self.findAdjacentSquare(player.opponent.ownBoard, target, direction)

                # If there is an adjacent square and the shot is a hit, and the adjacent square
                # has not been fired upon and is not already in the priorityList, add the
                # square to the priorityList
                if (adjacentSquare != None) and \
                        (player.opponent.ownBoard[target[0]][target[1]].getShip() != None) and \
                        (adjacentSquare in player.unfiredSquares) and \
                        (adjacentSquare not in player.prioritySquares):
                    player.prioritySquares.append(adjacentSquare)

    def getTarget(self, player, shotsLeft):
        '''Determines what they AI will shoot at this turn.
        Returns target as a (r, c) tuple.'''

        # Priority squares have the highest priority.
        # they will be fired upon before any other square.
        if len(player.prioritySquares) != 0:
            target = player.prioritySquares[randrange(len(player.prioritySquares))]
            player.prioritySquares.remove(target)

        # If there are no priority squares, the AI will fire at one of its preffered
        # squares
        elif len(player.favoriteSquares) != 0:
            target = player.favoriteSquares[randrange(len(player.favoriteSquares))]
            player.favoriteSquares.remove(target)

        # If neither of the above happens, it will fire at a random square
        else:
            target = player.unfiredSquares[randrange(len(player.unfiredSquares))]

        # Ensures that a targeted priority square will also be removed from the
        # favoriteSquares list.
        if target in player.favoriteSquares:
            player.favoriteSquares.remove(target)

        # unfiredSquares needs to me managed regardless of what target is
        player.unfiredSquares.remove(target)

        # Determines whether anything needs to be added to the priorityList
        self.thinkAbout(player, target)

        # This commented out code is useful when building new AI characters
        # print target, player.prioritySquares, len(player.unfiredSquares)
        return target