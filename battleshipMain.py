# Battleship.py
# A game about naval tactics and a bit of shoddy guesswork
# Kilian Roberts, CS111

from random import *

# Credit to John Zelle for creating graphics.py
# and David Liben-Nowell for creating the file from which
# modTypeAndClick is derived.
from game_pieces import *
from player_classes import *
from game_manager import *
from text_ui import *
from hybrid_ui import *
from  gui import *

#__________________________________MAIN____________________________________
            
def main():
    # Sets the game's interface
    response = input('\nWould you like to use the text-based UI, a GUI, or a hybrid of the two?\n>>> ').upper()
    if len(response) > 0 and response[0] == 'T':
        interface = textUI()
    elif len(response) > 0 and response[0] == 'H':
        interface = hybridUI()
    else:
        interface = GUI()
    
    # Creates a game manager that uses the program's interface
    GM = gameManager(interface)
    
    # Sets initial values: People want to play another game, they
    # don't want to use players from the nonexistent last game,
    # so the players are currently None
    continueYN = True
    samePlayersYN = False
    player1, player2 = None, None
    
    # Shows the title screen
    interface.showTitle()
    
    # If the gameManager returns a True continueYN, or if no game has
    # been played, the game manager runs a new game.
    while continueYN:
        # player1 and player2 are returned by the GM and given to the GM
        # so that players can be preserved between games if needed.
        continueYN, player1, player2 = GM.runGame(player1, player2)
        
if __name__ == '__main__':
    main()