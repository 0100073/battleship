class converter():
    '''The converter converts alphanumeric square names
    to (r,c) form'''

    def __init__(self):
        self.name = 'Converter'

    def __repr__(self):
        return self.name

    def convert(self, inputString):
        '''Used by the text UI to convert a user-entered alphanumeric
        square name to a (r,c) tuple square name.
        Returns None for invalid input.'''

        target = [None, None]

        # Does some preliminary formatting if the string is long enough
        # to be a valid input
        if (len(inputString) > 1):
            # Strips a space from the end of the input
            if inputString[-1] == ' ':
                inputString = inputString[:-1]
            # Strips the 1 in 10 from the beginning of the input
            # so that the lower section works converts 10 to 9
            if inputString[:2] == '10' or inputString[0] == ' ':
                inputString = inputString[1:]

            # for each position in the input...
            for i in (0, -1):

                # If the coordinate is a number, it describes the column.
                # The index of each character corresponds to its place in
                # the (r,c) coordinate system.
                if inputString[i] in '1234567890':
                    target[1] = '1234567890'.index(inputString[i])

                # If the coordinate is a letter, it describes the row.
                # The index of each character corresponds to its place in
                # the (r,c) coordinate system.
                elif inputString[i] in 'ABCDEFGHIJK':
                    target[0] = 'ABCDEFGHIJK'.index(inputString[i])

        # The function returns None for invalid input
        if None not in target:
            # Converts the target from a list to a tuple and returns it.
            return (target[0], target[1])