from game_pieces import *
from random import randrange
from aiui import AIBrain

class player:
    '''This class contains data and functions that all players need to have'''

    def __init__(self):
        # self.name will be set later.
        self.name = None
        # AI players have their own class
        self.playerType = 'Human'

        # Creates a self.ownBoard, a self.enemyBoard (displayed to the player),
        # and a fleet of ships for the player
        self.resetBoard()

        # These are set later
        self.opponent = None
        self.interface = None

    def __repr__(self):
        return self.name

    def resetBoard(self):
        '''Resets the liveShips and deadShips lists so that
        the same player objects can be used between games.
        Is also called when initially creating the player objects'''
        self.liveShips = []
        self.deadShips = []

        # Creates a set of ships for the player
        self.unplacedShips = [ship(2, 'Patrol Boat'),
                              ship(3, 'Submarine'),
                              ship(3, 'Destroyer'),
                              ship(4, 'Battleship'),
                              ship(5, 'Carrier')]

        # Creates board to hold/track the player's board
        # and to represent the enemy's board
        self.ownBoard = self.createBoard()
        self.enemyBoard = self.createBoard()

    def setName(self, name):
        self.name = name

    def setLanguage(self, language):
        self.language = language

    def setOpponent(self, opponent):
        self.opponent = opponent

    def setInterface(self, interface):
        self.interface = interface

    def getLiveShips(self):
        return self.liveShips

    def getPlayerType(self):
        return self.playerType

    def getName(self):
        return self.name

    def getUnplacedShips(self):
        return self.unplacedShips

    def getOwnBoard(self):
        return self.ownBoard

    def getEnemyBoard(self):
        return self.enemyBoard

    def createBoard(self):
        '''Used by __init__ to create self.ownboard
        and self.enemyBoard'''

        board = []

        # Creates squares to form a 10 by 10 grid of squares
        for i in range(10):
            board.append([])
            for j in range(10):
                # (i,j) are the square's coords in (r,c) form.
                board[i].append(square((i, j)))

        return board

    def checkRoom(self, ship, start, direction):
        '''This function is used by self.placeShip() to determine
        whether or not a ship can be placed as specified.
        Start is a tuple in (r,c) form.'''

        # Starts by doing basic checks

        # If the starting position is not within the grid, return False
        if not ((0 <= start[1] <= 9) and (0 <= start[0] <= 9)):
            return False

        # If there is not enough room for the ship in the grid, return false
        if ((direction == 'D' and start[0] + ship.getLength() > 10) or
                (direction == 'U' and start[0] - ship.getLength() < -1) or
                (direction == 'L' and start[1] - ship.getLength() < -1) or
                (direction == 'R' and start[1] + ship.getLength() > 10)):
            return False

        # If there is another ship in the way, return False
        for i in range(ship.getLength()):
            if (direction == 'D') and (self.ownBoard[start[0] + i][start[1]].hasShip()):
                return False
            elif (direction == 'U') and (self.ownBoard[start[0] - i][start[1]].hasShip()):
                return False
            elif (direction == 'L') and (self.ownBoard[start[0]][start[1] - i].hasShip()):
                return False
            elif (direction == 'R') and (self.ownBoard[start[0]][start[1] + i].hasShip()):
                return False

        # If no test fails, there is room for the ship.
        return True

    def fire(self, target):
        '''Hits the target square on the enemy's board
        and records the result on self.enemyBoard.
        Input must be in the form of a list or tuple in (r,c) form'''

        targetSquare = self.opponent.ownBoard[target[0]][target[1]]

        # Fires on enemy board, which returns True for hit and False for miss.
        hitTrue = targetSquare.hit()

        # Records the result on self.enemyBoard and may remove a dead ship from
        # opponent's unplacedShips list.
        if hitTrue:
            # Records the HMN on self.enemyBoard
            self.enemyBoard[target[0]][target[1]].setHitMissNeither('Hit')

            # Removes a dead ship from the opponent's liveShips list
            enemyShip = self.opponent.ownBoard[target[0]][target[1]].getShip()
            if (not enemyShip.isAlive()) and (enemyShip in self.opponent.liveShips):
                self.opponent.liveShips.remove(enemyShip)
                self.opponent.deadShips.append(enemyShip)

        else:
            # Set the relevant self.enemyboard square to 'Miss'
            self.enemyBoard[target[0]][target[1]].setHitMissNeither('Miss')

    def placeShip(self, ship, start, direction):
        '''Places a ship by taking the starting coordinates and a direction
        (d, r, u, or l).
        Returns whether or not the ship has been placed as a Boolean value'''

        # If there is enough space for the ship, place the ship in each
        # applicable block depending on the direction of the ship
        if self.checkRoom(ship, start, direction):
            for i in range(ship.getLength()):
                if direction == 'D':
                    self.ownBoard[start[0] + i][start[1]].setShip(ship)
                elif direction == 'U':
                    self.ownBoard[start[0] - i][start[1]].setShip(ship)
                elif direction == 'R':
                    self.ownBoard[start[0]][start[1] + i].setShip(ship)
                elif direction == 'L':
                    self.ownBoard[start[0]][start[1] - i].setShip(ship)

            # Lets us know that the ship has been placed
            self.unplacedShips.remove(ship)
            self.liveShips.append(ship)

            # If the ship has been placed return True.
            return True

        # If the ship has not been placed return False
        else:
            return False


class AI(player):
    '''This subclass of player is used to define the AI specific
    __init__ and resetBoard methods'''

    def __init__(self):
        self.possibleNames = ['Joshua', 'GLaDOS', 'H.A.L.', 'Skynet']
        self.name = self.possibleNames[randrange(len(self.possibleNames))]
        self.prioritySquares = []
        self.favoriteSquares = []

        # Creates ships, boards, and resets the unfiredSquares list
        self.resetBoard()

        self.playerType = 'AI'
        self.interface = AIBrain(self.prioritySquares, self.favoriteSquares, self.unfiredSquares)
        self.opponent = None
        self.language = 'NA'

    def resetBoard(self):
        '''Resets the liveShips, deadShips, favoriteSquares,
        unfiredSquares, and prioritySquares lists so that
        the same player objects can be used between games.
        Is also called when initially creating the player objects'''
        self.liveShips = []
        self.deadShips = []
        self.prioritySquares = []

        # Creates a set of ships for the player
        self.unplacedShips = [ship(2, 'Patrol Boat'),
                              ship(3, 'Submarine'),
                              ship(3, 'Destroyer'),
                              ship(4, 'Battleship'),
                              ship(5, 'Carrier')]

        # Creates the ownBoard and enemyBoard
        self.ownBoard = self.createBoard()
        self.enemyBoard = self.createBoard()

        # Keeps track of which squares have not been fired upon.
        self.unfiredSquares = [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6), (0, 7), (0, 8), (0, 9),
                               (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8), (1, 9),
                               (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (2, 7), (2, 8), (2, 9),
                               (3, 0), (3, 1), (3, 2), (3, 3), (3, 4), (3, 5), (3, 6), (3, 7), (3, 8), (3, 9),
                               (4, 0), (4, 1), (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7), (4, 8), (4, 9),
                               (5, 0), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7), (5, 8), (5, 9),
                               (6, 0), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7), (6, 8), (6, 9),
                               (7, 0), (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7), (7, 8), (7, 9),
                               (8, 0), (8, 1), (8, 2), (8, 3), (8, 4), (8, 5), (8, 6), (8, 7), (8, 8), (8, 9),
                               (9, 0), (9, 1), (9, 2), (9, 3), (9, 4), (9, 5), (9, 6), (9, 7), (9, 8), (9, 9)]

        # Skynet preffers to fire only on half of the squares for efficiency's sake.
        if self.name == 'Skynet':
            self.favoriteSquares = [(0, 0), (0, 2), (0, 4), (0, 6), (0, 8),
                                    (1, 1), (1, 3), (1, 5), (1, 7), (1, 9),
                                    (2, 0), (2, 2), (2, 4), (2, 6), (2, 8),
                                    (3, 1), (3, 3), (3, 5), (3, 7), (3, 9),
                                    (4, 0), (4, 2), (4, 4), (4, 6), (4, 8),
                                    (5, 1), (5, 3), (5, 5), (5, 7), (5, 9),
                                    (6, 0), (6, 2), (6, 4), (6, 6), (6, 8),
                                    (7, 1), (7, 3), (7, 5), (7, 7), (7, 9),
                                    (8, 0), (8, 2), (8, 4), (8, 6), (8, 8),
                                    (9, 1), (9, 3), (9, 5), (9, 7), (9, 9)]

        # GLaDOS always checks the corners first.
        # Her tests shows that people like to put ships there.
        elif self.name == 'GLaDOS':
            self.favoriteSquares = [(0, 0), (0, 1), (1, 0), (1, 1),
                                    (9, 9), (8, 9), (9, 8), (8, 8),
                                    (9, 0), (9, 1), (8, 0), (8, 1),
                                    (0, 9), (1, 9), (0, 8), (1, 8),
                                    (4, 4), (4, 5), (5, 5), (5, 4)]

        # HAL's name is short enough that it can be fired into the board
        elif self.name == 'H.A.L.':
            self.favoriteSquares = [(2, 0), (2, 1), (2, 3), (3, 0), (4, 0), (3, 2), (4, 2), (0, 0), (1, 0), (0, 2),
                                    (1, 2),
                                    (3, 3), (3, 4), (3, 5), (4, 3), (4, 5), (5, 3), (5, 4), (5, 5), (6, 3), (6, 5),
                                    (7, 3), (7, 5),
                                    (6, 6), (7, 6), (8, 6), (9, 6), (9, 7), (9, 8), (9, 9)]

        # Joshua doesn't really care what he shoots at
        else:
            self.favoriteSquares = []