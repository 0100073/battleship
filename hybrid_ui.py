from graphics import *
from converter import *

class hybridUI:
    '''This UI displays info in a GraphWin but requires
    that player enter input into the terminal'''

    def __init__(self):
        # This converts alphanumeric coordinates into
        # (r,c) form.
        self.converter = converter()

        # Creates the graphics window.
        self.window = GraphWin('Battleship', 600, 800)
        self.window.setBackground('black')

        # I used a curtain because I don't know how to undraw objects properly.
        self.curtain = Rectangle(Point(0, 0), Point(self.window.getWidth(), self.window.getHeight()))
        self.curtain.setOutline('black')
        self.curtain.setFill('black')

        # The title is displayed at the top of the screen,
        # just in case people forget what game they're playing
        self.title = Text(Point(300, 50), 'BATTLESHIP')
        self.title.setFill('green')
        self.title.setSize(36)
        self.title.setFace('courier')
        self.ownBoardStart = Point(210, 410)
        self.enemyBoardStart = Point(210, 130)
        self.messages = []

    def __repr__(self):
        return "A hybrid GUI/CLI for battleshipMain.py"

    def closed(self):
        '''This function is used to keep the game
        from crashing if the graphics window is closed'''
        if self.window.closed:
            return True
        else:
            return False

    def obscure(self):
        '''This function draws over all displayed objects.
        If I understood how to properly undraw objects I would
        do that instead.'''
        self.title = Text(Point(300, 50), 'BATTLESHIP')
        self.title.setFill('green')
        self.title.setSize(36)
        self.title.setFace('courier')

        self.curtain = Rectangle(Point(0, 0), Point(self.window.getWidth(), self.window.getHeight()))
        self.curtain.setOutline('black')
        self.curtain.setFill('black')

        if not self.closed():
            self.curtain.draw(self.window)
            self.title.draw(self.window)

    def makeBlank(self):
        '''Undraws all drawn objects.'''
        # for item in self.window.items:
        #    item.undraw()
        #    self.window.update()

    def highMessage(self, text):
        '''Draws a question on the window.
        Used by determineGameMode and determinePlayers'''
        question = Text(Point(300, 450), text)
        question.setFill('green')
        question.setSize(14)
        question.setFace('courier')
        if not self.closed():
            question.draw(self.window)
        print(text)

    def showTitle(self):
        '''Displays the title screen'''
        # Oh, if I understood graphics...
        self.makeBlank()
        self.curtain.draw(self.window)

        # I wanted the BATTLESHIP to be displayed lower on the title
        # screen than in the rest of the program
        tempTitle = Text(Point(300, 150), 'BATTLESHIP!')
        tempTitle.setFill('green')
        tempTitle.setSize(36)
        tempTitle.setFace('courier')
        tempTitle.draw(self.window)

        # Draws the subtitle
        subTitle = Text(Point(300, 450), 'A GAME OF NAVAL STRATEGY!\n(and a bit of shoddy guesswork)')
        subTitle.setFill('green')
        subTitle.setSize(24)
        subTitle.setFace('courier')
        subTitle.draw(self.window)

        self.lowMessage('Enter commands into the terminal.\nPress enter to continue.')
        input("\n>>> ")

    def determineGameMode(self):
        '''Sets the gamemode to either Classic or Salvo mode.
        In Salvo mode you get one shot for each of your
        living ships.'''

        if not self.closed():
            self.makeBlank()
            self.obscure()

        self.highMessage(
            "\nWould you like to play a Classic or Salvo game?\n(In a Salvo game you have 1 shot per live ship.)")

        response = input('>>> ').upper()

        # The game defaults to Classic mode
        if len(response) > 0 and response[0] == 'S':
            return 'Salvo'
        else:
            return 'Classic'

    def playerTypes(self):
        '''Determines whether each player is AI or Human'''
        # sigh...
        if not self.closed():
            self.makeBlank()
            self.obscure()

        self.highMessage('Should player 1 be a human or an AI?')

        response = input('>>> ').upper()

        # The game defaults to AI players.
        if (len(response) > 0 and response[0] == 'H') \
                or (len(response) > 2 and response[:3] == 'A H'):
            player1Type = 'Human'
        else:
            player1Type = 'Computer'

        if not self.closed():
            self.makeBlank()
            self.obscure()

        # This function is still repetitive.
        self.highMessage('Should player 2 be a human or an AI?')

        response = input('>>> ').upper()

        if (len(response) > 0 and response[0] == 'H') \
                or (len(response) > 2 and response[:3] == 'A H'):
            player2Type = 'Human'
        else:
            player2Type = 'Computer'

        return player1Type, player2Type

    def playerName(self, playerString):
        '''Asks the player for their name, in order to bring a more
        personal touch to the game'''

        if not self.closed():
            self.makeBlank()
            self.obscure()

        self.highMessage('What is your name, %s?' % (playerString))

        return input('>>> ')

    def lowMessage(self, text):
        '''Displays a given message at the bottom of the graphics
        window.'''

        # This curtain obscures previous messages
        # I wish that I had a more elegant solution.
        messageCurtain = Rectangle(Point(0, self.window.getHeight() - 180),
                                   Point(self.window.getWidth(), self.window.getHeight()))
        messageCurtain.setOutline('black')
        messageCurtain.setFill('black')

        # Formats the message.
        message = Text(Point(310, 700), text)
        message.setFill('green')

        # Makes longer messages smaller so that
        # they fit on the screen.
        if len(text) < 65:
            message.setSize(15)
        else:
            message.setSize(12)
        message.setFace('courier')
        # Prevents the program from crashing if the window has been closed.
        if not self.closed():
            messageCurtain.draw(self.window)
            message.draw(self.window)
        print(text)
        self.messages.append(message)

    def printBoard(self, board, startPoint):
        '''draws a given board on the screen.
        Does not obscure first'''
        # print(startPoint.getX(), startPoint.getY())

        # These variables are used to keep track
        # of a positions and objects
        xCounter = 0
        yCounter = 0
        objectsToDraw = []

        # These variables are used to easily
        # change the format the board and text
        WH = 19
        hmOffset = 10
        hmSize = 14

        # This draws the board and row letters
        for row in board:
            xCounter = 0
            yCounter += WH

            # Prints the row labels.
            # The labeling code takes advantage of
            # int to ASCII conversion to print the row labels
            sideLabel = Text(Point(startPoint.getX() - hmOffset,
                                   startPoint.getY() + yCounter + hmOffset),
                             chr(yCounter // WH + 64))
            sideLabel.setFill('green')
            sideLabel.setFace('courier')
            sideLabel.setSize(hmSize - 2)
            objectsToDraw.append(sideLabel)

            # Prints the board, row by row.
            for square in row:
                # Prints the column labels
                if yCounter == WH:
                    topLabel = Text(Point(startPoint.getX() + xCounter + hmOffset,
                                          startPoint.getY() + yCounter - hmOffset),
                                    str(xCounter // WH + 1))
                    topLabel.setFill('green')
                    topLabel.setFace('courier')
                    topLabel.setSize(hmSize - 2)
                    objectsToDraw.append(topLabel)

                # This creates the rectangle that represents each square in
                # the graphics window.
                point1 = Point(startPoint.getX() + xCounter, startPoint.getY() + yCounter)
                point2 = Point(point1.getX() + WH, point1.getY() + WH)
                squareGraphic = Rectangle(point1, point2)
                squareGraphic.setOutline('green')
                objectsToDraw.append(squareGraphic)

                # Used for ownBoard to indicate the location of one's ships
                if square.getShip() != None:
                    squareGraphic.setFill(color_rgb(125, 125, 125))

                # Marks hit ships
                if square.getHitMissNeither() == 'Hit':
                    x = Text(Point(point1.getX() + hmOffset, point1.getY() + hmOffset), 'X')
                    x.setFill('green')
                    x.setSize(hmSize)
                    objectsToDraw.append(x)

                # Marks shots that failed to hit a ship.
                if square.getHitMissNeither() == 'Miss':
                    o = Text(Point(point1.getX() + hmOffset, point1.getY() + hmOffset), 'O')
                    o.setFill('green')
                    o.setSize(hmSize)
                    objectsToDraw.append(o)

                xCounter += WH

        for item in objectsToDraw:
            item.draw(self.window)

    def showBoard(self, player):
        """Prints both of a player's boards"""

        if not self.closed():
            self.makeBlank()
            self.obscure()

        # Labels the enemyBoard
        enemyLabel = Text(Point(self.enemyBoardStart.getX() + 80,
                                self.enemyBoardStart.getY() - 25),
                          "\n%s's board:" % (player.opponent))
        enemyLabel.setFill('green')
        enemyLabel.setSize(18)
        enemyLabel.setFace('courier')
        if not self.closed():
            enemyLabel.draw(self.window)

        # Labels the ownBoard
        ownLabel = Text(Point(self.ownBoardStart.getX() + 80,
                              self.ownBoardStart.getY() - 25),
                        "\n%s's board:" % (player))
        ownLabel.setFill('green')
        ownLabel.setSize(18)
        ownLabel.setFace('courier')
        if not self.closed():
            ownLabel.draw(self.window)

            # Prints each board, at the position specified in __init_
            self.printBoard(player.getEnemyBoard(), self.enemyBoardStart)
            self.printBoard(player.getOwnBoard(), self.ownBoardStart)

    def findShip(self, shipName, player):
        '''Takes a ship's name and returns the ship.
        This function is used by selectShip'''

        for ship in player.getUnplacedShips():
            possibleMatchName = ship.getName()
            possibleMatchName = possibleMatchName.upper()
            # This allows the player to only type in the first
            # letter of the ship's name
            if possibleMatchName[0] == shipName[0]:
                return ship

    def selectShip(self, player):
        '''Prompts a user to choose which ship to place.
        Returns the ship.
        Used by placeShip.'''

        message = "%s. Which ship would you like to place?\nYour options are ..." % (player.getName())

        # Creates a list to store the names of each unplaced ship
        shipNameList = []

        for ship in player.getUnplacedShips():
            # Prints the name and length of each unplaced ship and adds
            # it to the shipNameList
            message = message + '\n' + "%s (length: %i)" % (ship, ship.getLength())
            shipNameList.append(ship.getName().upper()[0])

        self.lowMessage(message)
        response = input('\n>>> ').upper()

        # Checks to see if the response is valid and promts the user
        # to try again if the previous response was invalid
        while (len(response) < 1) or (response[0] not in shipNameList):
            message = "Please try again %s. Which ship would you like to place?\nYour options are ..." % (
            player.getName())
            for ship in player.getUnplacedShips():
                # Prints the name and length of each unplaced ship and adds
                # it to the shipNameList
                message = message + '\n' + "%s (length: %i)" % (ship, ship.getLength())
                shipNameList.append(ship.getName().upper()[0])
            self.lowMessage(message)
            response = input('\n>>> ').upper()


            # Once the program has a valid response, it finds and returns the ship
        ship = self.findShip(response, player)
        return ship

    def placeShip(self, player):
        '''Prompts the user to place a ship and returns the ship, the
        place it has been placed, and the direction it faces.'''

        # Prompts the user to select a ship.
        ship = self.selectShip(player)

        # Prompts the user to enter a ship starting location and a direction
        self.lowMessage(
            'Where would you like to place your %s?\nPlease enter a square and a direction(r, l, u, d) \nExample: A3 r' % (
            ship))
        response = input('\n>>> ').upper()

        # If the length of the response is too short, direction is set to None
        # so that another part of the program will reject the given placement
        if len(response) >= 2:
            direction = response[-1]
        else:
            direction = None

        # Converts the response to a (r,c) tuple.
        # The converter returns None if it can't convert
        # the input
        # response[:-1] sends only the coordinate to the converter
        start = self.converter.convert(response[:-1])

        # If the input is invalid, the user is prompted to try again.
        while (start == None) or (direction not in 'RULDruld'):
            self.lowMessage(
                'Please try again.\nWhere would you like to place your %s?\nPlease enter a square and a direction(r, l, u, d) \nExample: A3 r' % (
                ship))

            response = input('\n>>> ').upper()

            if len(response) >= 2:
                direction = response[-1].lower()
            else:
                direction = None

            start = self.converter.convert(response[:-1])

        # Once a valid square is entered, the ship, start, and direction
        # are returned
        return ship, start, direction

    def getTarget(self, player, shotsLeft):
        '''Prompts the player to enter a target, runs
        the result through the converter and returns
        it as an (r, c) tuple.'''

        if shotsLeft > 1:
            self.lowMessage("\n%s. Enter a target. \nYou have %s shots left" % (player, shotsLeft))
        else:
            self.lowMessage('Enter a target, %s.' % (player))
        response = input('>>> ').upper()

        # The converter takes the alphanumeric input
        # and translates it into an (r,c) tuple
        target = self.converter.convert(response)

        # If the converter is unable to translate
        # the input, it returns None.
        while target == None:
            if shotsLeft > 1:
                self.lowMessage("\n%s. Enter a target. \nYou have %s shots left" % (player, shotsLeft))
            else:
                self.lowMessage('Enter a target, %s.' % (player))
            response = input('>>> ').upper()
            target = self.converter.convert(response)

        return target

    def endTurn(self, player):
        '''Prompts the player to end their turn'''

        self.lowMessage('%s, press Enter to end your turn.' % (player))
        input('>>> ')

    def startTurn(self, player):
        '''Prompts the player to end their turn.
        Used in Human vs Human games'''

        self.lowMessage("%s, press Enter to start your turn." % (player))
        input('>>> ')

    def victory(self, player):
        '''Prints the victory screen'''

        if not self.closed():
            self.makeBlank()
            self.obscure()

        self.lowMessage("\n%s has won the game!\nPress Enter to continue" % (player))

        # Lets you see everyone's board
        victorLabel = Text(Point(self.enemyBoardStart.getX() + 80,
                                 self.enemyBoardStart.getY() - 25),
                           "\n%s's board:" % (player))
        victorLabel.setFill('green')
        victorLabel.setSize(18)
        victorLabel.setFace('courier')
        if not self.closed():
            victorLabel.draw(self.window)

        loserLabel = Text(Point(self.ownBoardStart.getX() + 80,
                                self.ownBoardStart.getY() - 25),
                          "\n%s's board:" % (player.opponent))
        loserLabel.setFill('green')
        loserLabel.setSize(18)
        loserLabel.setFace('courier')
        if not self.closed():
            loserLabel.draw(self.window)

        if not self.closed():
            self.printBoard(player.ownBoard, self.enemyBoardStart)
            self.printBoard(player.opponent.ownBoard, self.ownBoardStart)
        input('>>> ')

    def askYN(self, question):
        '''Asks the user a yes/no question'''

        self.lowMessage("\n%s (y/n)" % (question))
        response = input('>>> ').upper()
        if (len(response) > 0) and (response[0] == 'Y'):
            return True
        else:
            return False