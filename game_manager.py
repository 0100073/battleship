from text_ui import *
from player_classes import *
from gui import *
from hybrid_ui import *

class gameManager:
    '''gameManager manages each game of battleship.'''

    def __init__(self, interface):
        self.gameNumber = 0
        # gameManager receives the interface from main()
        self.interface = interface
        # gameManager defaults to classic mode
        self.gameMode = 'Classic'
        # Prevents bugs in checkInterface
        self.player1 = None
        self.player2 = None

    def __repr__(self):
        return "Game management object for battleship.py"

    def setGameMode(self, gameMode):
        self.gameMode = gameMode

    def checkInterface(self):
        '''Checks to see if the interface
        reports it is closed. If so, it
        switches to a Text-based UI'''
        if self.interface.closed():
            self.interface = textUI()

            for player in [self.player1, self.player2]:
                if (player != None) and (player.getPlayerType() == 'Human'):
                    player.setInterface(self.interface)

    def determinePlayers(self):
        '''Determines what type of player each player is
        and set each as the other's opponent'''

        self.checkInterface()
        # Determines what type of player each player is
        p1Type, p2Type = self.interface.playerTypes()

        if p1Type == 'Human':
            self.player1 = player()
            # Asks the interface for the player's name and sets
            # that as their name. Some UI's will just use 'Player1'
            # or 'Player2', hence the string argument.
            self.player1.setName(self.interface.playerName('Player 1'))
            # This is here so that AI's don't call the UI
            self.player1.setInterface(self.interface)
        else:
            self.player1 = AI()

        if p2Type == 'Human':
            self.player2 = player()
            # Asks the interface for the player's name and sets
            # that as their name. Some UI's will just use 'Player1'
            # or 'Player2', hence the string argument.
            self.player2.setName(self.interface.playerName('Player 2'))
            # This is here so that AI's don't call the UI
            self.player2.setInterface(self.interface)
        else:
            self.player2 = AI()

        # This enables each player's fire functions to determine
        # whether or not they hit the other player's ships
        self.player1.setOpponent(self.player2)
        self.player2.setOpponent(self.player1)

        # The players are stored as self. so that checkInterface
        # can change their interfaces.
        return self.player1, self.player2

    def runGame(self, player1, player2):
        '''Runs a single game. main() initially passes player1
        and player 2 as None to let the function know that it
        must have the user determine the player type.
        The function returns the player objects, casuing bugs
        that need to be resolved.'''

        # -----------------Determining_Game_Mode----------

        self.checkInterface()
        self.setGameMode(self.interface.determineGameMode())

        # -----------------Determining_Players------------
        self.checkInterface()
        # If either of the players hasn't been defined,
        # both players are reset.
        if player1 == None or player2 == None:
            player1, player2 = self.determinePlayers()
        else:
            player1.resetBoard()
            player2.resetBoard()

        # -----------------Placing_Ships------------------

        # As long as a player has unplaced ships, they are
        # prompted to place another ship.
        while (len(player1.getUnplacedShips()) != 0):
            self.checkInterface()
            player1.interface.showBoard(player1)
            ship, start, direction = player1.interface.placeShip(player1)

            # If the player does not place the ship correctly,
            # they are informed that ship placement has failed.
            if not player1.placeShip(ship, start, direction):
                player1.interface.lowMessage('Placement Failed')

        self.checkInterface()
        # Prevents player2 from accidentally seeing player1's ships
        if player1.getPlayerType() == 'Human' and player2.getPlayerType() == 'Human':
            self.interface.obscure()

        # As long as a player has unplaced ships, they are
        # prompted to place another ship.
        while (len(player2.getUnplacedShips()) != 0):
            self.checkInterface()
            player2.interface.showBoard(player2)
            ship, start, direction = player2.interface.placeShip(player2)

            # If the player does not place the ship correctly,
            # they are informed that ship placement has failed.
            if not player2.placeShip(ship, start, direction):
                player2.interface.lowMessage('Placement Failed')

        self.checkInterface()
        if player1.getPlayerType() == 'Human' and player2.getPlayerType() == 'Human':
            self.interface.obscure()

        # -----------------Running_The_Game---------------
        # turnCounter is used to alternate between player1 and player2
        turnCounter = 0

        # If a player has no live ships left, they have lost.
        while (len(player1.getLiveShips()) != 0) and (len(player2.getLiveShips()) != 0):

            # This alternates turns between player1 and player2
            if turnCounter % 2 == 0:
                playerWithTurn = player1
            else:
                playerWithTurn = player2

            self.checkInterface()

            # Constantly asking players to start their turn gets annoying
            # and is not needed when playing against an AI
            if player1.getPlayerType() == 'Human' and player2.getPlayerType() == 'Human':
                playerWithTurn.interface.startTurn(playerWithTurn)

            self.checkInterface()
            # Display's the player's boards
            playerWithTurn.interface.showBoard(playerWithTurn)

            # In Salvo mode, a player gets a shot for each of their live ships.
            if self.gameMode == 'Salvo':
                numShots = len(playerWithTurn.getLiveShips())
            # In Classic mode, you always get one shot.
            else:
                numShots = 1

            # For as many shots as a player gets, the player fires at the enemy board
            for i in range(numShots):
                self.checkInterface()

                target = playerWithTurn.interface.getTarget(playerWithTurn, numShots - i)
                playerWithTurn.fire(target)
                self.checkInterface()
                # The player then gets to see what they hit
                playerWithTurn.interface.showBoard(playerWithTurn)

            # The player's turn then ends
            playerWithTurn.interface.endTurn(playerWithTurn)

            self.checkInterface()

            # Constantly obscuring the screen gets annoying
            # and is not needed when playing against an AI
            if playerWithTurn.getPlayerType() == 'Human' and \
                            playerWithTurn.opponent.getPlayerType() == 'Human':
                playerWithTurn.interface.obscure()

            # Updates the turn counter to switch whose turn it is.
            turnCounter += 1

        # ---------------Determining_The_Winner-----------

        # At this point, the game has ended

        self.checkInterface()
        if len(player1.getLiveShips()) != 0:
            self.interface.victory(player1)

        elif len(player2.getLiveShips()) != 0:
            self.interface.victory(player2)

        # ---------------Whether_To_Continue--------------

        self.checkInterface()

        # askYN returns the user's answer to the question as a Boolean
        continueYN = self.interface.askYN('Would you like to play another game?')

        if continueYN:
            self.checkInterface()
            samePlayersYN = self.interface.askYN('Would you like keep the same players?')
        else:
            samePlayersYN = False

        # If the user wants to keep the same players, the players
        # are returned to main and given to the next instance of the
        # runGame method.
        if samePlayersYN:
            return continueYN, player1, player2
        else:
            return continueYN, None, None